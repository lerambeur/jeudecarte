import java.awt.Color;
import java.awt.Image;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;

import javax.swing.*;
import javax.swing.border.Border;

/**
 *
 */
public class AfficheMainJoueur implements MouseListener{

	private JPanel panelCarteJoueur;
	
	private JButton btnCarteSelect;
	
	private ArrayList<JButton> listBtn;

	private int numBtnCarteSelect;

	/**
	 *
	 * @param panelCarteJoueur
	 */
	public AfficheMainJoueur(JPanel panelCarteJoueur) {
		this.panelCarteJoueur = panelCarteJoueur;
		listBtn = new ArrayList<>();
	}

	/**
	 *
	 * @param numBtnCarteSelect
	 */
	public void setNumBtnCarteSelect(int numBtnCarteSelect) {
		this.numBtnCarteSelect = numBtnCarteSelect;
	}

	/**
	 *
	 * @return
	 */
	public boolean isCarteSelect() {
		return btnCarteSelect != null;
	}

	/**
	 *
	 * @return
	 */
	public int getNumBtnCarteSelect() {
		return numBtnCarteSelect;
	}

	/**
	 *
	 */
	public void setBtnCarteSelect() {
		this.btnCarteSelect = null;
	}

	/**
	 *
	 * @param mainJoueur
	 * @param nomJoueur
	 */
	public void afficheMainJoueur(ArrayList<Carte> mainJoueur, String nomJoueur) {

		panelCarteJoueur.removeAll();
		listBtn= new ArrayList<>();

		JLabel labelNomJoueur = new JLabel("Joueur "+nomJoueur.toUpperCase());
		labelNomJoueur.setBounds(550, 20,150, 90);
		panelCarteJoueur.add(labelNomJoueur);

		for (int i = 0; i < mainJoueur.size(); i++) {
			JButton carte = new JButton("");
			Image img = new ImageIcon(mainJoueur.get(i).getImageCarteDate()).getImage().getScaledInstance(150, 200, Image.SCALE_DEFAULT);
			carte.setIcon(new ImageIcon(img));
			carte.setBounds(placeCarte(mainJoueur.size(), i + 1), 100, 150, 200);
			carte.addMouseListener(this);
			listBtn.add(carte);
			panelCarteJoueur.add(carte);
		}
		this.panelCarteJoueur.repaint();
	}

	/**
	 *
	 * @param cartePose
	 * @param numeroCarte
	 * @return
	 */
	private int placeCarte(int cartePose, int numeroCarte) {
		int position = (900 - (11 * (cartePose - 1) + (cartePose * 100))) / 2;
		position += (numeroCarte - 1) * 175;
		return position;
	}

	/**
	 *
	 * @param coor
	 */
	private void numCarte(int coor) {
		for(int i=0 ; i< listBtn.size() ; i++) {
			if(placeCarte(listBtn.size(),i+1) == coor) {
				this.setNumBtnCarteSelect(i);
			}
		}
	}

	/**
	 *
	 * @param e
	 */
	@Override
	public void mouseClicked(MouseEvent e) {
		for (JButton btn : listBtn) {
			if (e.getComponent().equals(btn)) {
				if (btnCarteSelect != null && btnCarteSelect.equals(btn)) {
					JButton btn2 = (JButton) e.getComponent();
					btn2.setBorder(null);
					btn2.setBounds(e.getComponent().getX(), 100, 150, 200);
					this.btnCarteSelect = null;
				} else if (btnCarteSelect != null) {
					Border border = BorderFactory.createLineBorder(java.awt.Color.green, 3);
					JButton btn2 = (JButton) e.getComponent();
					btn2.setBorder(border);
					btn2.setBounds(e.getComponent().getX(), 100, 150, 200);
					numCarte(e.getComponent().getX());
					this.btnCarteSelect = btn2;
					for (JButton btn3 : listBtn) {
						if (!btn3.equals(btnCarteSelect)) {
							btn3.setBorder(null);
							btn3.setBounds(btn3.getX(), 100, 150, 200);
						}
					}
				} else {
					Border border = BorderFactory.createLineBorder(Color.green, 3);
					JButton btn3 = (JButton) e.getComponent();
					btn3.setBorder(border);
					btn3.setBounds(e.getComponent().getX(), 100, 150, 200);
					numCarte(e.getComponent().getX());
					btnCarteSelect = btn3;
				}
			}
		}
		
	}

	/**
	 *
	 * @param e
	 */
	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
	}

	/**
	 *
	 * @param e
	 */
	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
	}

	/**
	 *
	 * @param arg0
	 */
	@Override
	public void mousePressed(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	/**
	 *
	 * @param arg0
	 */
	@Override
	public void mouseReleased(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

}
