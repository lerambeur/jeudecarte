/**
 *
 */

import java.util.ArrayList;

/**
 *
 */
public class Joueur {

	private String nomJoueur;
	private int ageJoueur;
	private ArrayList<Carte> mainJoueur = new ArrayList<Carte>();
	private boolean isNotElimine;

	/**
	 *
	 * @param nomJoueur
	 * @param ageJoueur
	 */
	public Joueur(String nomJoueur, int ageJoueur) {
		this.nomJoueur = nomJoueur;
		this.ageJoueur = ageJoueur;
		this.isNotElimine=true;
	}

	/**
	 *
	 * @return
	 */
	public String getNomJoueur() {
		return nomJoueur;
	}

	/**
	 *
	 * @return
	 */
	public int getAgeJoueur() {
		return ageJoueur;
	}

	/**
	 *
	 * @return
	 */
	public ArrayList<Carte> getMainJoueur() {
		return mainJoueur;
	}

	/**
	 *
	 * @return
	 */
	public boolean isNotElimine() {
		return isNotElimine;
	}

	/**
	 *
	 * @param isNotElimine
	 */
	public void setNotElimine(boolean isNotElimine) {
		this.isNotElimine = isNotElimine;
	}

	/**
	 *
	 * @param carte
	 */
	public void addMainJoueur(Carte carte) {
		mainJoueur.add(carte);
	}

	/**
	 *
	 * @param position
	 * @return
	 */
	public Carte recupPosCarte(int position) {
		return mainJoueur.get(position);
	}

	/**
	 *
	 * @param position
	 */
	public void supprPosCarte(Carte position) {
		this.mainJoueur.remove(position);
	}

	
}