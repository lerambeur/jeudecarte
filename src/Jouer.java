import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;

/**
 *
 */
public class Jouer {
	private ArrayList<Carte> pioche = new ArrayList<>();
	private ArrayList<Carte> cartePoser;
	
	private Comparator<Carte> carteComparator;

	/**
	 *
	 * @throws IOException
	 */
	public Jouer() throws IOException {
		chargePioche();

		this.cartePoser=new ArrayList<>();
		this.carteComparator = new CompareDate();
	}


	/**
	 *
	 * @return
	 */
	public Carte pioche() {
		Carte distribution = pioche.get(0);
		pioche.remove(0);
		return distribution;
	}

	/**
	 *
	 * @param carte
	 */
	public void addCartePoser(Carte carte) {
		this.cartePoser.add(carte);
		Collections.sort(this.cartePoser, carteComparator);
	}

	public ArrayList<Carte> getCartePoser() {
		return cartePoser;
	}

	/**
	 *
	 * @throws IOException
	 */
	public void chargePioche() throws IOException {
		BufferedReader bufferedReader = new BufferedReader(new FileReader("data/timeline/timeline.csv"));
		String line = "";
		while ((line = bufferedReader.readLine()) != null) {
			String[] mot = line.split(";");
			Carte carte = new Carte(mot[0], strToInt(mot[1]), mot[2]);
			pioche.add(carte);
		}
		pioche.remove(0);
		Collections.shuffle(pioche);
	}

	/**
	 *
	 * @param annee
	 * @return
	 */
	public static int strToInt(String annee) {
		int i = 0;
		int num = 0;
		boolean isNeg = false;

		if (annee.charAt(0) == '-') {
			isNeg = true;
			i = 1;
		}

		while (i < annee.length()) {
			num *= 10;
			num += annee.charAt(i++) - '0';
		}

		if (isNeg)
			num = -num;
		return num;
	}

	/**
	 *
	 * @param joueur
	 */
	public void piocheCarte(Joueur joueur) {
		joueur.getMainJoueur().add(pioche.get(0));
		pioche.remove(0);
	}

	/**
	 *
	 * @param joueurs
	 */
	public void ordreJoueur(ArrayList<Joueur> joueurs) {

		Joueur premier;
		ArrayList<Joueur> trier = new ArrayList<>();
		trier.addAll((Collection<? extends Joueur>) joueurs.clone());
		trier.sort(Comparator.comparing(Joueur::getAgeJoueur).thenComparing(Joueur::getNomJoueur));
		premier = trier.get(0);


		ArrayList<Joueur> joueursFinal = new ArrayList<>();
		joueursFinal.add(premier);


		for (int i = joueurs.indexOf(premier) + 1; i < joueurs.size(); i++) {
			joueursFinal.add(joueurs.get(i));
		}
		for (int i = 0; i < joueurs.indexOf(premier); i++) {
			joueursFinal.add(joueurs.get(i));
		}
		joueurs.clear();
		joueurs.addAll(joueursFinal);

	}

	/**
	 *
	 * @param joueurs
	 */
	public void distribution(ArrayList<Joueur> joueurs) {
		if (joueurs.size() == 2 || joueurs.size() == 3) {
			for (Joueur joueur : joueurs) {
				for (int j = 0; j < 6; j++) {
					piocheCarte(joueur);
				}
				//System.out.println(joueur.getMainJoueur().toString());
			}
		} else if (joueurs.size() == 4 || joueurs.size() == 5) {
			for (Joueur joueur : joueurs) {
				for (int j = 0; j < 5; j++) {
					piocheCarte(joueur);
				}
				//System.out.println(joueur.getMainJoueur().size());
			}
		} else if (joueurs.size() >= 6 && joueurs.size() <= 8) {
			for (Joueur joueur : joueurs) {
				for (int j = 0; j < 4; j++) {
					piocheCarte(joueur);
				}
				//System.out.println(joueur.getMainJoueur().size());
			}
		}
	}

	/**
	 *
	 * @param joueurs
	 * @return
	 */
	public boolean verifNextTour(ArrayList<Joueur> joueurs) {
		for (Joueur joueur : joueurs) {
			if (joueur.getMainJoueur().size() == 0) {
				return true;
			}
		}
		return false;
	}

	/**
	 *
	 * @param joueurs
	 */
	public void verifCarte(ArrayList<Joueur> joueurs) {
		for(Joueur joueur : joueurs) {
			if(joueur.getMainJoueur().size()>1) {
				joueur.setNotElimine(false);
			}
		}
	}

	/**
	 *
	 * @param carte
	 * @param position
	 * @return
	 */
	public boolean verifPoseCarte(Carte carte, int position) {
		int pos = Collections.binarySearch(cartePoser, carte, carteComparator);
		if (pos < 0) {
			pos = -pos - 1;
			return pos == position;
		} else {
			return pos == position || pos + 1 == position;
		}
	}

	/**
	 *
	 * @param n
	 * @param joueurs
	 * @return
	 */
	public int nextPlayer(int n, ArrayList<Joueur> joueurs) {

		if (n == joueurs.size() - 1) {
			n = 0;
			while (joueurs.get(n).isNotElimine() == false) {
				n++;
			}
		} else {
			n++;
			while (joueurs.get(n).isNotElimine() == false) {
				if (n == joueurs.size() - 1) {
					n = 0;
				} else {
					n++;
				}

			}
		}
		return n;
	}

	/**
	 *
	 * @param joueurs
	 */
	public void disqualifie(ArrayList<Joueur> joueurs) {
		for (Joueur joueur : joueurs) {
			if (joueur.getMainJoueur().size() != 0 && joueur.isNotElimine() == true) {
				joueur.setNotElimine(false);
			}
			if (joueur.getMainJoueur().size() == 0 && joueur.isNotElimine()) {
				joueur.addMainJoueur(pioche());
			}
		}
	}

	/**
	 *
	 * @param joueurs
	 * @return
	 */
	public boolean verifGagne(ArrayList<Joueur> joueurs) {
		int compteur = 0;
		for (Joueur joueur : joueurs) {
			if (joueur.isNotElimine())
				compteur++;
		}
		return compteur == 1;
	}
}
