import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;

/**
 *
 */
public class Accueil extends JFrame {
    JPanel panelAccueil = new javax.swing.JPanel();
    JTextField textNomJoueur = new javax.swing.JTextField();
    JTextField textAgeJoueur = new javax.swing.JTextField();
    JLabel labelNomJoueur = new javax.swing.JLabel();
    JLabel labelAgeJoueur = new javax.swing.JLabel();
    JLabel labelListeJoueur = new javax.swing.JLabel();
    JList lJoueurs = new javax.swing.JList<>();
    JButton bAjouter = new javax.swing.JButton();
    JButton bJouer = new javax.swing.JButton();

    private ArrayList<Joueur> joueurs = new ArrayList<>();

    private DefaultListModel defaultListModel = new DefaultListModel();

    public ArrayList<Joueur> getJoueurs() {
        return joueurs;
    }

    /**
     *
     * @param args
     */
    public static void main(String[] args) {
        new Accueil();
    }

    /**
     *
     */
    public Accueil() {
        initialize();
    }

    /**
     *
     */
    private void initialize(){
        panelAccueil.setLayout(null);

        labelNomJoueur.setLabelFor(textNomJoueur);
        labelAgeJoueur.setLabelFor(textAgeJoueur);
        labelListeJoueur.setLabelFor(lJoueurs);

        labelNomJoueur.setText("Nom du joueur : ");
        labelNomJoueur.setBounds(40, 50, 120, 30);
        textNomJoueur.setBounds(150, 50, 90, 30);

        labelAgeJoueur.setText("Age du joueur : ");
        labelAgeJoueur.setBounds(40, 100, 120, 30);
        textAgeJoueur.setBounds(150, 100,90, 30);

        bAjouter.setText("Ajouter");
        bAjouter.setBounds(70, 150, 150, 50);
        bAjouter.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                addPlayer();
                if (joueurs.size()>1 && joueurs.size()<9){
                    bJouer.setEnabled(true);
                }
            }
        });

        labelListeJoueur.setText("Liste joueurs : ");
        labelListeJoueur.setBounds(40, 220, 120, 30);
        lJoueurs.setBounds(140, 220, 100, 160);

        bJouer.setText("Jouer");
        bJouer.setBounds(70, 400, 150, 50);
        bJouer.setEnabled(false);
        bJouer.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    new Plateau(joueurs);
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
                dispose();

            }
        });

        panelAccueil.add(labelNomJoueur);
        panelAccueil.add(textNomJoueur);

        panelAccueil.add(labelAgeJoueur);
        panelAccueil.add(textAgeJoueur);

        panelAccueil.add(bAjouter);

        panelAccueil.add(labelListeJoueur);
        panelAccueil.add(lJoueurs);

        panelAccueil.add(bJouer);

        this.setTitle("Accueil");
        this.add(panelAccueil);
        this.setResizable(false);
        this.setVisible(true);
        this.setBounds(900, 300, 300, 500);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

    }


    /**
     *
     */
    public void addPlayer() {
        if (!textNomJoueur.getText().isEmpty() && !textAgeJoueur.getText().isEmpty()){
            joueurs.add(new Joueur(textNomJoueur.getText(), Integer.parseInt(textAgeJoueur.getText())));
            defaultListModel.addElement(textNomJoueur.getText()+" "+textAgeJoueur.getText());
            lJoueurs.setModel(defaultListModel);
        } else {
            JOptionPane.showMessageDialog(this, "Saisir nom et âge du joueur ");
        }

        textNomJoueur.setText("");
        textAgeJoueur.setText("");
        textNomJoueur.requestFocus();

    }






}
