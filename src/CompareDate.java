import java.util.Comparator;

/**
 *
 */
public class CompareDate implements Comparator<Carte>{
	/**
	 *
	 * @param carte1
	 * @param carte2
	 * @return
	 */
	public int compare(Carte carte1, Carte carte2) {
		if (carte1.getDateCarte() < carte2.getDateCarte()) {
			return -1;
		}
		if (carte1.getDateCarte() > carte2.getDateCarte()) {
			return 1;
		}
		return 0;
	}
	
}
