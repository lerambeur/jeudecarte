/**
 *
 */
public class Carte {
	String nomCarte;
	int dateCarte;
	String imageCarte;

	/**
	 *
	 * @param nomCarte
	 * @param dateCarte
	 * @param imageCarte
	 */
	public Carte(String nomCarte, int dateCarte, String imageCarte) {
		super();
		this.nomCarte = nomCarte;
		this.dateCarte = dateCarte;
		this.imageCarte = imageCarte;
	}

	/**
	 *
	 * @return
	 */
	public String getNomCarte() {
		return nomCarte;
	}

	/**
	 *
	 * @return
	 */
	public int getDateCarte() {
		return dateCarte;
	}

	/**
	 *
	 * @return
	 */
	public String getImageCarte() {
		return "data/timeline/cards/"+imageCarte+".jpeg";
	}

	/**
	 *
	 * @return
	 */
	public String getImageCarteDate() {
		return "data/timeline/cards/"+imageCarte+"_date.jpeg";
	}

	
	
	
}
