import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.*;

/**
 *
 */
public class Plateau implements ActionListener {

	private JFrame plateau;
	private JPanel panelCarteJoueur;
	private JPanel panelCartePoser;
	private JScrollPane scrollPane;
	private ArrayList<JButton> addBtn;
	private ArrayList<Joueur> joueurs;
	private int posJoueur;
	private Jouer jouer;
	private AfficheMainJoueur mainJoueur;

	/**
	 *
	 * @param joueurs
	 * @throws IOException
	 */
	public Plateau(ArrayList<Joueur> joueurs) throws IOException {
		init();

		this.posJoueur = 0;
		this.joueurs = joueurs;
		this.addBtn = new ArrayList<JButton>();

		this.mainJoueur = new AfficheMainJoueur(panelCarteJoueur);
		this.panelCarteJoueur.setLayout(null);

		this.jouer = new Jouer();

		this.jouer.addCartePoser(this.jouer.pioche());
		this.jouer.ordreJoueur(joueurs);
		this.jouer.distribution(joueurs);

		affichePlateau();
	}

	/**
	 *
	 */
	private void init() {
		this.plateau = new JFrame();
		this.plateau.setVisible(true);
		plateau.setResizable(false);
		this.plateau.setBounds(100, 100, 1400, 900);
		this.plateau.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.plateau.getContentPane().setLayout(null);

		this.panelCarteJoueur = new JPanel();
		this.panelCarteJoueur.setLayout(null);
		this.panelCarteJoueur.setBounds(10, 450, 1300, 400);
		this.panelCarteJoueur.setBackground(Color.white);
		this.plateau.getContentPane().add(this.panelCarteJoueur);

		this.scrollPane = new JScrollPane();
		this.scrollPane.setBounds(10, 10, 1300, 400);
		this.plateau.getContentPane().add(this.scrollPane);

		this.panelCartePoser = new JPanel();
		this.scrollPane.setViewportView(this.panelCartePoser);
		this.panelCartePoser.setBackground(Color.WHITE);

	}

	/**
	 *
	 */
	private void affichePlateau() {

		this.panelCartePoser.removeAll();
		this.addBtn=  new ArrayList<JButton>();

		int position = 0;

		for (Carte carte : this.jouer.getCartePoser()) {
			JButton btnadd = new JButton("+");
			btnadd.setBounds(5 + 153 * position, 275, 20, 30);
			panelCartePoser.add(btnadd);


			JButton btnCartePoser = new JButton("");
			Image image = new ImageIcon(carte.getImageCarteDate()).getImage().getScaledInstance(150, 200, Image.SCALE_DEFAULT);
			btnCartePoser.setIcon(new ImageIcon(image));
			btnCartePoser.setBorder(null);
			btnCartePoser.setBounds(153 * position + 50,80, 150, 200);

			panelCartePoser.add(btnCartePoser);

			position++;

			btnadd.addActionListener(this);
			addBtn.add(btnadd);
		}
		JButton btnadd = new JButton("+");
		btnadd.setBounds(5 + 153 * position, 275, 30, 30);
		btnadd.addActionListener(this);
		panelCartePoser.add(btnadd);
		addBtn.add(btnadd);


		this.mainJoueur.afficheMainJoueur(joueurs.get(this.posJoueur).getMainJoueur(),this.joueurs.get(this.posJoueur).getNomJoueur());

		this.panelCartePoser.revalidate();
	}

	/**
	 *
	 * @param e
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		int position = -1;
		int pos = 0;
		for (JButton b : addBtn) {
			if (e.getSource().equals(b)) {
				position = pos;
			}
			pos++;
		}
		if (this.mainJoueur.isCarteSelect()) {
			Carte carte = joueurs.get(this.posJoueur).recupPosCarte(this.mainJoueur.getNumBtnCarteSelect());
			if (jouer.verifPoseCarte(carte, position)) {
				this.mainJoueur.setBtnCarteSelect();
				this.jouer.addCartePoser(carte);
				joueurs.get(this.posJoueur).supprPosCarte(carte);

				if(joueurs.get(this.posJoueur).getMainJoueur().size()==0) {
					this.jouer.verifCarte(joueurs);
				}
				if (this.jouer.nextPlayer(this.posJoueur, joueurs) < this.posJoueur) {
					if (this.jouer.verifNextTour(joueurs)) {
						this.jouer.disqualifie(joueurs);
					}
				}
			} else{
				this.mainJoueur.setBtnCarteSelect();
				joueurs.get(this.posJoueur).supprPosCarte(carte);
				joueurs.get(this.posJoueur).addMainJoueur(jouer.pioche());
				if (this.jouer.nextPlayer(this.posJoueur, joueurs) < this.posJoueur) {
					if (this.jouer.verifNextTour(joueurs)) {
						this.jouer.disqualifie(this.joueurs);
					}
				}
			}
			if (this.jouer.verifGagne(this.joueurs)) {
				JOptionPane.showMessageDialog(plateau, "Bravooo vous avez gagné");
				plateau.dispose();
			} else {
				this.posJoueur = this.jouer.nextPlayer(this.posJoueur, joueurs);
				affichePlateau();
			}
		}

	}
}
